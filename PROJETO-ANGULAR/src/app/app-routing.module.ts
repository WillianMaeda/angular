import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {EventoFormComponent} from './eventos/evento-form/evento-form.component';
import {EventosListaComponent} from './eventos/eventos-lista/eventos-lista.component';
import {UpdateEventosComponent} from './eventos/update-eventos/update-eventos.component';
import {ConvidadoFormComponent} from './convidados/convidado-form/convidado-form.component';
import {ConvidadosListaComponent} from './convidados/convidados-lista/convidados-lista.component';
import {SobreComponent} from './menu/sobre/sobre.component';
import {UpdateConvidadosComponent} from './convidados/update-convidados/update-convidados.component';
import {LoginComponent} from './login/usuario-login/login.component';
import {AuthGuard} from './auth.guard';

const routes: Routes = [
  {path: 'form', component: EventoFormComponent, canActivate : [AuthGuard]},
  {path: 'listaEventos', component: EventosListaComponent, canActivate : [AuthGuard]},
  {path: 'editar/:codigo', component: UpdateEventosComponent, canActivate : [AuthGuard]},
  {path: 'convidadoform', component: ConvidadoFormComponent, canActivate : [AuthGuard]},
  {path: 'listaConvidados', component: ConvidadosListaComponent, canActivate : [AuthGuard]},
  {path: 'sobre', component: SobreComponent},
  {path: 'atualizar/:codigo', component: UpdateConvidadosComponent, canActivate : [AuthGuard]},
  {path: 'login', component: LoginComponent}

]

@NgModule({
  exports: [ RouterModule ],
  imports: [RouterModule.forRoot(routes)]
})
export class AppRoutingModule {}
