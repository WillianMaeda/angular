import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateConvidadosComponent } from './update-convidados.component';

describe('UpdateConvidadosComponent', () => {
  let component: UpdateConvidadosComponent;
  let fixture: ComponentFixture<UpdateConvidadosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateConvidadosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateConvidadosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
