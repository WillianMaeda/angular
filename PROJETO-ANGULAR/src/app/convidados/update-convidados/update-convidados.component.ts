import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Convidado } from '../Convidado';
import { ConvidadoService } from '../Convidado-Service';

@Component({
  selector: 'app-update-convidados',
  templateUrl: './update-convidados.component.html',
  styleUrls: ['./update-convidados.component.css']
})
export class UpdateConvidadosComponent implements OnInit {

  convidado : Convidado = new  Convidado();



  codigo= this.actRoute.snapshot.params['codigo'];
 convidados:Convidado[] = [];

 constructor(private service: ConvidadoService, public actRoute: ActivatedRoute,
    public router: Router) { }

  ngOnInit() {
    const isIdPresent = this.actRoute.snapshot.paramMap.has('codigo');
    if (isIdPresent) {
      const codigo = +this.actRoute.snapshot.paramMap.get('codigo');
      this.service.listarPorCodigo(codigo).subscribe(
        data => this.convidado = data 
      )
  }
  }

  salvar(convidado)
  {
  this.service.salvar(this.convidado).subscribe( convidadoSalvado => 
    { this.convidados.push(convidadoSalvado) 
      console.log(convidadoSalvado);
    
    })
    
    
    console.log("Teste", convidado);
  }


}
