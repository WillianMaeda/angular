import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Convidado } from './Convidado';

@Injectable
({
    providedIn : 'root'
})
export class ConvidadoService {

    apiUrl: string = environment.apiURLBase + '/api/convidados';

    constructor(private http: HttpClient) {}

    salvar(convidado: Convidado)
    {
        return this.http.post<Convidado>(`${this.apiUrl}`, convidado);
    }

    listar(): Observable<Convidado[]>
    {
        return this.http.get<Convidado[]>(this.apiUrl);
    }

    remover(codigo: number): Observable<any> {
        return this.http.delete(`${this.apiUrl}/${codigo}`);

    }

    listarPorCodigo(codigo: number) : Observable<Convidado>
    {
        return this.http.get<any>(`${this.apiUrl}/${codigo}`);
    }

}