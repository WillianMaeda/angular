import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConvidadosListaComponent } from './convidados-lista.component';

describe('ConvidadosListaComponent', () => {
  let component: ConvidadosListaComponent;
  let fixture: ComponentFixture<ConvidadosListaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConvidadosListaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConvidadosListaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
