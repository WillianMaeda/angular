import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Convidado } from '../Convidado';
import { ConvidadoService } from '../Convidado-Service';

@Component({
  selector: 'app-convidados-lista',
  templateUrl: './convidados-lista.component.html',
  styleUrls: ['./convidados-lista.component.css']
})
export class ConvidadosListaComponent implements OnInit {

  convidado: Convidado;
  convidados: Convidado[];
  mensagemSucesso: string;
  mensagemErro: string;
  sucess : boolean = false;
  errors: String[];

  constructor(private service: ConvidadoService, private router: Router) { }

  ngOnInit() {
    this.listarTodosConvidados();
  }

  listarTodosConvidados()
  {
  this.service.listar().subscribe( todos => {
    console.log(todos);
    this.convidados = todos;

  });
  }

  remover(convidado : Convidado)
  {
    this.service.remover(convidado.codigo).subscribe
       (     data => {
         this.convidados = this.convidados.filter( e => e !== convidado);
       })
   
  }

}
