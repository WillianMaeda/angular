import { Component } from '@angular/core';
import { MenuItem } from 'primeng/components/common/menuitem';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';

  items: MenuItem[];

  ngOnInit() {
      this.items = [
        {
          label:'PAGINA INICIAL',
          url: '/',
      },
          {
              label:'CADASTRAR EVENTOS',
              url: '/form',
          },
          {
              label:'LISTAR EVENTOS',
              url: '/listaEventos',
          },
          {
              label:'CADASTRAR CONVIDADO',
              url: '/convidadoform',
          },
          {
              label:'LISTAR CONVIDADO',
              url: '/listaConvidados',

           },
          {
              label:'SOBRE',
              url: '/sobre',
          },
          {
            label:'LOGIN',
            url: '/login',
        }
      ];
  }    

  }    

