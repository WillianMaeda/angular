import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { InputTextModule } from 'primeng/inputtext';

import { AppComponent } from './app.component';
import { EventoFormComponent } from './eventos/evento-form/evento-form.component';
import { EventosListaComponent } from './eventos/eventos-lista/eventos-lista.component';
import {CardModule} from 'primeng/card';
import {ButtonModule} from 'primeng/button';
import {TableModule} from 'primeng/table';
import { FormsModule }   from '@angular/forms';
import { EventoService } from './eventos/Evento-Service';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppRoutingModule } from './/app-routing.module';
import { UpdateEventosComponent } from './eventos/update-eventos/update-eventos.component';
import { ConvidadoFormComponent } from './convidados/convidado-form/convidado-form.component';
import { ConvidadosListaComponent } from './convidados/convidados-lista/convidados-lista.component';
import {MenubarModule} from 'primeng/menubar';
import { SobreComponent } from './menu/sobre/sobre.component';
import { LoginComponent } from './login/usuario-login/login.component';
import { UpdateConvidadosComponent } from './convidados/update-convidados/update-convidados.component';
import { ConvidadoService } from './convidados/Convidado-Service';
import { UsuarioService } from './login/Usuario-service';
import { TokenInterceptor } from 'src/token.interceptor';


@NgModule({
  declarations: [
    AppComponent,
    EventoFormComponent,
    EventosListaComponent,
    UpdateEventosComponent,
    ConvidadoFormComponent,
    ConvidadosListaComponent,
    SobreComponent,
    LoginComponent,
    UpdateConvidadosComponent

  ],
  imports: [
    BrowserModule,
    CardModule,
    ButtonModule,
    TableModule,
    FormsModule,
    InputTextModule,
    HttpClientModule,
    AppRoutingModule,
    MenubarModule,
  ],
  providers: [ EventoService,
    ConvidadoService, UsuarioService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
