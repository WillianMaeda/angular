import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Evento } from '../Evento';
import { EventoService } from '../Evento-Service';

@Component({
  selector: 'app-eventos-lista',
  templateUrl: './eventos-lista.component.html',
  styleUrls: ['./eventos-lista.component.css']
})
export class EventosListaComponent implements OnInit {

  evento: Evento;
  eventos:Evento[] = [];
  //EventoSelecionado : Evento;
  mensagemSucesso: string;
  mensagemErro: string;
  sucess : boolean = false;
  errors: String[];
  
  constructor(private service: EventoService, private router: Router ) {}

  ngOnInit(): void {
this.listarTodos();
    
  }


   listarTodos()
   {
   this.service.listar().subscribe( todos => {
     console.log(todos);
     this.eventos = todos;
 
   });
   }

     remover(evento : Evento)
     {
       this.service.remover(evento.codigo).subscribe
          (     data => {
            this.eventos = this.eventos.filter( e => e !== evento);
          })
      
     }


  }


  
// this.users = this.users.filter(u => u !== user);

