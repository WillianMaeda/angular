import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateEventosComponent } from './update-eventos.component';

describe('UpdateEventosComponent', () => {
  let component: UpdateEventosComponent;
  let fixture: ComponentFixture<UpdateEventosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateEventosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateEventosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
