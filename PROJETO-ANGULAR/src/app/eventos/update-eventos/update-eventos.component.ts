import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Evento } from '../Evento';
import { EventoService } from '../Evento-Service';

@Component({
  selector: 'app-update-eventos',
  templateUrl: './update-eventos.component.html',
  styleUrls: ['./update-eventos.component.css']
})
export class UpdateEventosComponent implements OnInit {

  evento: Evento = new Evento();

 // evento: Evento;
  codigo= this.actRoute.snapshot.params['codigo'];
  eventos:Evento[] = [];

  constructor(private service: EventoService, public actRoute: ActivatedRoute,
    public router: Router) { }

  ngOnInit() {
    const isIdPresent = this.actRoute.snapshot.paramMap.has('codigo');
    if (isIdPresent) {
      const codigo = +this.actRoute.snapshot.paramMap.get('codigo');
      this.service.listarPorCodigo(codigo).subscribe(
        data => this.evento = data 
      )
  }
}

salvar(evento)
{
this.service.salvar(this.evento).subscribe( salvado => 
  { this.eventos.push(salvado) 
    console.log(salvado);
  
  })
  
  
  console.log("Teste", evento);
}


}

/*
  editarEventos() {
    if(window.confirm('Are you sure, you want to update?')){
      this.service.atualizar(this.codigo, this.evento).subscribe
     (data => {
        this.router.navigate(['/'])
      })
    }
  }*/



  /*
   updateEmployee() {
    this.employeeService.updateEmployee(this.id, this.employee)
      .subscribe(data => {
        console.log(data);
        this.employee = new Employee();
        this.gotoList();
      }, error => console.log(error));
  }
  */
